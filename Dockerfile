FROM node:12-alpine
WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn install

COPY webpack webpack
COPY src src

CMD yarn start
