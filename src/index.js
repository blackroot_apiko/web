import { Application } from './Application'

import './styles.scss'

const root = document.createElement('div')
root.id = 'root'

root.appendChild(Application())

document.body.appendChild(root)
