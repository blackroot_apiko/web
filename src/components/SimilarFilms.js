import './styles.scss'

import { api } from 'services/api'

function GetCard (film) {
  this.id = film.id
  const card = document.createElement('div')

  const poster = document.createElement('img')
  poster.src = `http://image.tmdb.org/t/p/w200/${film.poster_path}`

  card.innerText = film.title
  card.appendChild(poster)

  card.onclick = () => {
    location.assign(this.id)
  }

  return card
}

const getSimilaryFilms = id => {
  const root = document.createElement('div')
  root.className = 'similary-films'

  api.getSimilary(id)
    .then(res => {
      return res.slice(0, res.length > 9 ? 9 : res.length)
    })
    .then(res => {
      for (const item of res) {
        const card = new GetCard(item)
        root.appendChild(card)
      }
    })

  return root
}

export { getSimilaryFilms }
