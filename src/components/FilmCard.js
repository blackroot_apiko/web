import './styles.scss'

const FilmCard = film => {
  const filmCard = document.createElement('div')
  filmCard.className = 'film-card'

  const h2Title = document.createElement('h2')
  h2Title.innerText = film.original_title
  h2Title.className = 'title'

  const poster = document.createElement('img')
  poster.src = `http://image.tmdb.org/t/p/w500/${film.poster_path}`

  filmCard.appendChild(h2Title)
  filmCard.appendChild(poster)

  filmCard.addEventListener('click', () => {
    handleClick(film.id)
  })

  return filmCard
}

const handleClick = id => {
  location.assign(id)
}

export { FilmCard }
