import { api } from 'services/api'

import { getSimilaryFilms } from './SimilarFilms'

import './styles.scss'

function FilmInformation (id) {
  const film = document.createElement('div')
  film.className = 'film-page'

  const title = document.createElement('h1')
  title.className = 'title'

  const section = document.createElement('div')
  section.className = 'section'
  const left = document.createElement('div')
  const right = document.createElement('div')

  const poster = document.createElement('img')
  const about = document.createElement('div')

  const overview = document.createElement('p')
  const productionCompanies = document.createElement('p')
  productionCompanies.innerHTML = '<b>Distributed by</b>: '

  const releaseDate = document.createElement('p')
  const voteAverage = document.createElement('p')

  api.loadingInformationAboutFilm(id)
    .then(res => {
      title.innerText = res.original_title
      poster.src = `http://image.tmdb.org/t/p/w500/${res.poster_path}`

      res.production_companies.map(company => {
        productionCompanies.innerHTML = productionCompanies.innerHTML + company.name + ' '
      })

      voteAverage.innerHTML = '<b>Vote average: </b>' + res.vote_average
      releaseDate.innerHTML = '<b>Release date: </b>' + res.release_date

      overview.innerHTML = '<b>Overview</b>: ' + res.overview
    })

  film.appendChild(title)
  left.appendChild(poster)

  right.appendChild(productionCompanies)
  right.appendChild(voteAverage)
  right.appendChild(releaseDate)
  right.appendChild(overview)
  right.appendChild(getSimilaryFilms(id))

  section.appendChild(left)
  section.appendChild(right)

  film.appendChild(section)

  film.appendChild(about)

  return film
}

export { FilmInformation }
