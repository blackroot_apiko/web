import { FilmCard } from './FilmCard'

import { api } from 'services/api'

import './styles.scss'

class FilmController {
  constructor () {
    this.films = []
    this.page = 1

    this.filmsDOM = document.createElement('div')
    this.filmsDOM.className = 'films'
    this.loadingMoreFilms(this.page)

    window.addEventListener('scroll', event => {
      if ((window.innerHeight + window.scrollY) >= this.filmsDOM.scrollHeight) {
        this.loadingMoreFilms()
      }
    })
  }

  loadingMoreFilms () {
    this.page++
    api.loadingPage(this.page)
      .then(res => {
        this.films = [...this.films, ...res]

        res.map(el => {
          const card = FilmCard(el)
          this.filmsDOM.appendChild(card)
        })
      })
  }

  getDOMElement () {
    return this.filmsDOM
  }
}

export { FilmController }
