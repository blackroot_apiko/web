import './styles.scss'

const getFooter = () => {
  const footer = document.createElement('div')
  footer.className = 'footer'

  footer.innerText = new Date().getFullYear()

  return footer
}

export { getFooter }
