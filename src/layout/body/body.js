import { FilmController, FilmInformation } from 'components'

import './styles.scss'

const getBody = () => {
  const body = document.createElement('div')
  body.className = 'body'

  const { pathname } = location

  if (pathname === '/') {
    const filmController = new FilmController()

    body.appendChild(filmController.getDOMElement())
  } else {
    const id = pathname.substring(1)
    body.appendChild(FilmInformation(id))
  }

  window.addEventListener('scroll', event => {
    if ((window.innerHeight + window.scrollY) >= body.scrollHeight) {
    }
  })

  return body
}

export { getBody }
