import './styles.scss'

const dropDown = document.createElement('div')

const handleClick = id => {
  location.assign(id)
}

const isVisible = elem => !!elem && !!(elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length)

const hideOnClickOutside = e => {
  if (!dropDown.contains(e.target) & isVisible(dropDown)) {
    const event = new Event('clear-drop-down')
    window.dispatchEvent(event)

    removeClickListener()
  }
}

const removeClickListener = () => {
  window.removeEventListener('click', () => {})
}

function getDropDown () {
  dropDown.className = 'drop-down'

  window.addEventListener('click', hideOnClickOutside)

  window.addEventListener('clear-drop-down', () => {
    dropDown.innerHTML = ''
    dropDown.style.padding = '0'
  })

  window.addEventListener('get-film-from-search', ({ detail }) => {
    dropDown.innerHTML = ''
    dropDown.style.padding = '20px'

    const { res } = detail

    res.map(el => {
      console.log(el)

      const item = document.createElement('div')
      item.className = 'item'

      item.addEventListener('click', () => {
        handleClick(el.id)
      })

      const poster = document.createElement('img')
      poster.src = `http://image.tmdb.org/t/p/w92/${el.poster_path}`

      const div = document.createElement('div')

      const title = document.createElement('h3')
      title.innerText = el.title

      const overview = document.createElement('p')
      overview.innerText = el.overview

      item.appendChild(poster)
      div.appendChild(title)
      div.appendChild(overview)

      item.appendChild(div)
      dropDown.appendChild(item)
    })
  })

  return dropDown
}

export { getDropDown }
