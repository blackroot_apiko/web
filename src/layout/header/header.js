import { getSearch } from './Search'

import './styles.scss'

const getHeader = () => {
  const header = document.createElement('div')
  header.className = 'header'

  header.appendChild(getSearch())

  return header
}

export { getHeader }
