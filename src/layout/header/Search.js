import { getDropDown } from './DropDown'

import { api } from 'services/api'

const input = document.createElement('input')
const button = document.createElement('button')

const handleClearSearch = e => {
  e.preventDefault()

  const event = new Event('clear-drop-down')
  window.dispatchEvent(event)

  input.value = ''
}

const handleChangeInput = e => {
  const value = e.target.value

  if (value.length > 3) {
    api.search(value)
      .then(res => {
        const event = new CustomEvent('get-film-from-search', { detail: { res: res } })
        window.dispatchEvent(event)
      })
  } else {
    const event = new Event('clear-drop-down')
    window.dispatchEvent(event)
  }
}

const getSearch = () => {
  const rootForm = document.createElement('div')
  rootForm.className = 'form-root'
  const form = document.createElement('form')
  form.className = 'form'

  button.innerText = 'CLear'

  button.addEventListener('click', handleClearSearch, false)
  input.addEventListener('input', handleChangeInput, false)

  form.appendChild(input)
  form.appendChild(button)

  rootForm.appendChild(form)
  rootForm.appendChild(getDropDown())

  return rootForm
}

export { getSearch }
