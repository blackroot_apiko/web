import { getHeader } from 'layout/header'
import { getBody } from 'layout/body'
import { getFooter } from 'layout/footer'

const getPage = () => {
  const page = document.createElement('div')
  page.className = 'page'

  page.appendChild(getHeader())
  page.appendChild(getBody())
  page.appendChild(getFooter())

  return page
}

const Application = () => {
  const page = getPage()

  return page
}

export { Application }
