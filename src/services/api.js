const TOKEN = process.env.TOKEN || 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwMmUwYWQ3MzEzYjZlNDYwNzc4NTlkYWRlZmYyMjgwMCIsInN1YiI6IjVlNjY3ODRkMzU3YzAwMDAxMTM5YWJmZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.4l4pP83GawLVd-NGUM1pKvkHTFA1lPi2RH5mSEFyRlI'

export const api = new class {
  constructor () {
    this.token = TOKEN
  }

  request (uri, options) {
    return fetch(`https://api.themoviedb.org/3${uri}`, options)
      .then(res => res.json())
  }

  get (uri, options = {}) {
    return this.request(uri, Object.assign({}, options, {
      method: 'GET',
      headers: Object.assign({}, options.headers || {}, {
        'Content-Type': 'application/json; charset=utf8',
        Authorization: this.token
      })
    }))
  }

  loadingPage (page) {
    return this.get(`/discover/movie?page=${page}`)
      .then(res => res.results)
  }

  loadingInformationAboutFilm (id) {
    return this.get(`/movie/${id}`)
      .then(res => res)
  }

  search (query) {
    return this.get(`/search/movie?query=${query}`)
      .then(res => res.results)
      .then(res => res.slice(15))
  }

  getSimilary (id) {
    return this.get(`/movie/${id}//similar`)
      .then(res => {
        console.log(res.results)

        return res.results
      })
  }
}()
