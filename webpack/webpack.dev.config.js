const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')

const ROOT = path.resolve(__dirname, '../')
const context = path.resolve(ROOT, 'src')

module.exports = {
  mode: process.env.MODE || 'development',
  watch: true,
  devtool: 'source-map',
  entry: path.join(context, 'index.js'),
  module: {
    rules: [
      {
        test: /\.js$/,
        use: [{
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env']
          }
        }]
      },
      {
        test: /\.scss$/,
        use: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Apiko'
    })
  ],

  resolve: {
    extensions: ['.js', '.json', '.scss'],
    modules: [
      path.resolve(context),
      'node_modules'
    ]
  },

  devServer: {
    historyApiFallback: true,
    port: process.env.PORT || 8000,
    host: '0.0.0.0',
    compress: true
  }
}
